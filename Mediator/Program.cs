﻿using System;

namespace Mediator
{
    //Сам интерфейс Медиатора
    public interface IMediator
    {
        void Send(string msg, Employee employee);
    }

    //Класс который взаймодействует с Медиатором
    public abstract class Employee
    {
        protected IMediator _mediator;


        public Employee(IMediator mediator)
        {
            this._mediator = mediator;
        }

        public virtual void Send(string message)
        {
            _mediator.Send(message, this);
        }
        public abstract void Notify(string message);
    }
    //Класс который обменивается с помощью Медиатора. В данный момент это сотрудники компани 
    class Dias : Employee
    {
        public Dias(IMediator mediator) : base(mediator)
        {
        }

        public override void Notify(string message)
        {
            Console.WriteLine("Финансовый отдел:  "  + message);
        }
    }

    class Noyan : Employee
    {
        public Noyan(IMediator mediator) : base(mediator)
        {
        }

        public override void Notify(string message)
        {
            Console.WriteLine("Отдел разработки:  " + message);
        }
    }

    class Duman : Employee
    {
        public Duman(IMediator mediator) : base(mediator)
        {
        }

        public override void Notify(string message)
        {
            Console.WriteLine("Отдел тестирование:  " + message);
        }
    }

    //Класс который дает всем задание и управляет всеми. Напрямую реализует Медиатор
    class Nurzhan : IMediator
    {
        public Employee Dias { get; set; }
        public Employee Noyan { get; set; }
        public Employee Duman { get; set; }

        public void Send(string msg, Employee employee)
        {
            if (Dias == employee)
                Noyan.Notify(msg);

            else if (Noyan == employee)
                Duman.Notify(msg);

            else if (Duman == employee)
                Dias.Notify(msg);
        }
    }
    //тетс коммит
    class Program
    {
        static void Main(string[] args)
        {
            Nurzhan mediator = new Nurzhan();
            Employee finansist = new Dias(mediator);
            Employee programmist = new Noyan(mediator);
            Employee tester = new Duman(mediator);

            mediator.Dias = finansist;
            mediator.Noyan = programmist;
            mediator.Duman = tester;

            finansist.Send("Деньги переведены, начинайте разработку:");
            programmist.Send("Продакшн и тест успешны. Можете продовать продукт");
            tester.Send("Проект продан, считаем прибль");

            Console.ReadKey();
        }
    }
}
